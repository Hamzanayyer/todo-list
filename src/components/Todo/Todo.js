import React, { useContext } from "react";
import styled from "styled-components";
import todosContext from "TodosContext";

const Button = styled.button`
  background-color: black;
  color: white;
  margin-left: 5px;
  max-height: 30px;
  margin-top: 5px;
`;

const Item = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;
`;
const Style = styled.li`
  list-style-type: none;
  border: 1px solid #ddd;
  margin-top: -1px;
  ${"" /* background-color: #f6f6f6; */}
  padding: 12px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Todo = ({ el }) => {
  const contextValue = useContext(todosContext);
  return (
    <Item>
      {el ? (
        <Style>
          {el}
          <Button onClick={() => contextValue.removeTodo(el)}>X</Button>
        </Style>
      ) : (
        ""
      )}
    </Item>
  );
};

export default Todo;
