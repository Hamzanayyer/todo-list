import React, { useContext } from "react";
import todosContext from "TodosContext";
import styled from "styled-components";
import Todo from "components/Todo/Todo";

const ItemContent = styled.div`
  font-family: Georgia, serif;
  margin-top: 20px;
`;

const TodoList = () => {
  const contextValue = useContext(todosContext);
  return (
    <ItemContent>
      {contextValue.items
        ? contextValue.items.map((el, index) => {
            return (
              <div key={index}>
                <Todo el={el} />
              </div>
            );
          })
        : null}
    </ItemContent>
  );
};

export default TodoList;
