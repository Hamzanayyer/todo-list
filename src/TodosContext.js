import React from "react";

export default React.createContext({
  items: [],
  setItems: (el) => {},
  value: "",
  setValue: (el) => {},
  handleInput: (el) => {},
  handleSubmit: (el) => {},
  addTodo: (el) => {},
  removeTodo: (el) => {},
});
