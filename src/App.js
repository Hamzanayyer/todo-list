import React from "react";
import TodoList from "components/todoList/TodoList";
import styled from "styled-components";
import FormTodo from "components/form/Form";
import TodosContext from "TodosContext";

const Title = styled.h1`
  text-align: center;
`;

const Container = styled.div`
  background: linear-gradient(#e66465, #9198e5);
  width: 50%;
  margin: 0 auto;
  padding-bottom: 10px;
  border-radius: 10px;
`;

function App() {
  const [items, setItems] = React.useState([]);
  const [value, setValue] = React.useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
  };

  const handleInput = (event) => {
    event.preventDefault();
    setValue(event.target.value.toUpperCase());
  };

  const addTodo = (event) => {
    event.preventDefault();
    setItems([...items, value]);
    setValue("");
  };

  const removeTodo = (item) => {
    const array = items;
    const index = array.indexOf(item);
    const newArray = items.filter((_, i) => i !== index);
    setItems(newArray);
  };

  const contextValue = {
    items,
    setItems,
    value,
    setValue,
    handleInput,
    handleSubmit,
    addTodo,
    removeTodo,
  };

  return (
    <TodosContext.Provider value={contextValue}>
      <Container>
        <Title>Todo List</Title>
        <FormTodo />
        <TodoList />
      </Container>
    </TodosContext.Provider>
  );
}

export default App;
