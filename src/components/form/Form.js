import React, { useContext } from "react";
import styled from "styled-components";
import todosContext from "TodosContext";

const Form = styled.form`
  display: flex;
  justify-content: center;
  height: 30px;
`;

const Button = styled.button`
  background-color: black;
  color: white;
  font-weight: bold;
  border: none;
  border-radius: 10%;
  margin-left: 5px;
  width: 100px;
`;

const FormTodo = () => {
  const contextValue = useContext(todosContext);
  return (
    <Form onSubmit={contextValue.handleSubmit}>
      <label htmlFor='item' />
      <input
        value={contextValue.value}
        onChange={contextValue.handleInput}
        placeholder='Add todo ..'
        id='item'
      />
      <Button className='submit' onClick={contextValue.addTodo}>
        Add
      </Button>
    </Form>
  );
};

export default FormTodo;
